Commerce Coupon Free Shipping
===============

Description
-----------

Commerce Coupon Free Shipping is a plugin for Commerce Coupon. This plugin 
will create a new Commerce Coupon Type and will make all processing for 
matched shipping value to 0.


Dependencies
------------

Drupal Commerce Coupon and all of its dependencies


Installation
------------

Commerce Coupon contains one module:

- Commerce Coupon Free Shipping

Enable it.
